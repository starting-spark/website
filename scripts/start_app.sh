#!/bin/sh

# This script starts the application

bundle exec jekyll serve \
  --host 0.0.0.0 \
  --livereload

