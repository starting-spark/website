FROM ruby:3.0.2 as development
ARG APP_DIR=/usr/src/app/
WORKDIR ${APP_DIR}/

RUN apt-get update && apt-get install -y \
    shellcheck \
  && rm -rf /var/lib/apt/lists/*

COPY Gemfile* ${APP_DIR}/
COPY scripts/install_dependencies.sh ${APP_DIR}/scripts/
RUN scripts/install_dependencies.sh

COPY . ${APP_DIR}


FROM development as builder
RUN scripts/build_app.sh


FROM registry.access.redhat.com/ubi8/ubi-minimal as production
ARG APP_DIR=/usr/src/app/
WORKDIR ${APP_DIR}/

#RUN microdnf update && microdnf install \
#  && microdnf clean all # this will error out if no packages are listed above for installation

COPY . ${APP_DIR}

