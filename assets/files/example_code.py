"""Help others start their spark

:param interested_coder: Anyone with an interest in coding
    (default is "Everyone")
:type interested_coder: str
:returns: Someone with a love of coding
:rtype: float
"""
from curiosity import experience


class StartingSpark():
    """Offers code coaching, workshops, & software consulting"""


    passion = "Start your Spark"
    mission = "Spark True"


    @classmethod
    def say_hi(cls):
        """Drop a note"""
        return "We Would love to meet you!"


