# Starting Spark Website
Jekyll project backing https://www.startingspark.com

[![pipeline status](https://gitlab.com/starting-spark/website/badges/master/pipeline.svg)](https://gitlab.com/starting-spark/website/-/commits/master)

Prerequisites
-------------
1. install docker
1. install docker-compose
1. install git
1. clone repository: `git clone --recursive https://gitlab.com/starting-spark/website.git`

Getting Started
---------------
1. run bootstrap.sh: `./bootstrap.sh`
1. start service: `docker compose up`

Testing
-------
To test the application:

    app$ scripts/test_app.sh

Linting
-------
To lint the shell scripts:

    $ docker compose run shell-checker sh
    shell-checker$ scripts/lint_app.sh
or
    $ docker-compose run shell-checker scripts/lint_app.sh

Documenting
-----------
To document the application:

    app$ scripts/document_app.sh

Notes
-----
TBD

